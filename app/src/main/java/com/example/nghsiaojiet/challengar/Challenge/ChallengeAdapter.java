package com.example.nghsiaojiet.challengar.Challenge;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.provider.Settings.Secure;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Toast;

import com.example.nghsiaojiet.challengar.Chat.ChatRoom;
import com.example.nghsiaojiet.challengar.Matches.ChatMatches;
import com.example.nghsiaojiet.challengar.Matches.MatchesObject;
import com.example.nghsiaojiet.challengar.QRActivity;
import com.example.nghsiaojiet.challengar.R;
import com.google.android.gms.common.internal.Constants;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.lang.ref.Reference;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ChallengeAdapter extends RecyclerView.Adapter<ChallengeViewHolder> implements Filterable {

    public ArrayList<Challenge> ChallengeList = new ArrayList<Challenge>();
    public ArrayList<Challenge> ChallengeListFiltered = new ArrayList<Challenge>();
    public Context context;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private String currentUid;
    private String currentEmail;
    private String userKey;



    public ChallengeAdapter(ArrayList<Challenge> challengeList, Context context){
        ChallengeList = challengeList;
        this.context = context;
        this.ChallengeListFiltered = ChallengeList;
        this.ChallengeList = ChallengeList;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if(charString.isEmpty()){
                    ChallengeListFiltered = ChallengeList;
                }
                else{
                    ArrayList<Challenge> filteredList = new ArrayList<>();
                    for(Challenge row : ChallengeList){
                            if(row.getTitle().toLowerCase().contains(charString.toLowerCase()) || row.getLocation().contains(charSequence)){
                                filteredList.add(row);
                            }
                    }
                    ChallengeListFiltered = filteredList;

                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = ChallengeListFiltered;
                return filterResults;

            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                ChallengeListFiltered = (ArrayList<Challenge>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    @NonNull
    @Override
    public ChallengeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(parent.getContext()).inflate(R.layout.challenge_item, parent, false);
        return new ChallengeViewHolder(item);
    }

    @Override
    public void onBindViewHolder(@NonNull final ChallengeViewHolder holder, int position) {
        final Challenge challenge = ChallengeListFiltered.get(position);
        holder.imgPicture.setImageResource(challenge.getChallengeImage());
        holder.txtTitle.setText(challenge.getTitle());
        holder.txtLocation.setText(challenge.getLocation());
        holder.txtPts.setText(String.valueOf(challenge.getPts()));



        holder.btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) { //matching function
                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Start Challenge?");
                mDatabase = FirebaseDatabase.getInstance().getReference();
                AlertDialog.Builder builder1 = builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mAuth = FirebaseAuth.getInstance();
                        currentEmail = mAuth.getCurrentUser().getEmail(); //use email into pending

                        //add one challenge into matches object
                        mDatabase.child("Matches").orderByChild("email").equalTo(currentEmail).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if(dataSnapshot.exists()){
                                    Toast.makeText(context, "You have already started a challenge. Finish it up before starting another challenge!", Toast.LENGTH_SHORT).show();
                                }
                                else{
                                    String Title = challenge.getTitle();
                                    MatchesObject match = new MatchesObject(currentEmail, Title, "firebase", "");
                                    mDatabase.child("Matches").push().setValue(match);
                                    final DatabaseReference matchesRef = mDatabase.child("Matches");
                                    ValueEventListener eventListener = new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            Log.d("Tag", "onDataChange: there is two person with same challenge!");
                                            String similarChallenge = challenge.getTitle();
                                            for(DataSnapshot ds : dataSnapshot.getChildren()){//skips this part
                                                Log.d("Tag", "onDataChange: goes through for loop");
                                                final String challengeTitle = ds.child("challenge").getValue(String.class);
                                                final String Name = ds.child("Name").getValue(String.class);
                                                if(similarChallenge.toLowerCase().equals(challengeTitle.toLowerCase())){ //if there are two challenges the same
                                                    final String otherUsersEmail = ds.child("email").getValue(String.class);
                                                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                                                    alertDialog.setTitle("Matched!");
                                                    alertDialog.setMessage("You have been matched with " + otherUsersEmail + " on the challenge " + challengeTitle);
                                                    alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialogInterface, int i) {
                                                            Toast.makeText(context, "Go to your chatroom and check for your new friend.", Toast.LENGTH_SHORT).show();
                                                            Context context = view.getContext();
                                                            Intent intent = new Intent(context, ChatMatches.class);
                                                            intent.putExtra("FriendEmail", otherUsersEmail);
                                                            intent.putExtra("similarchallenge", challengeTitle);
                                                            intent.putExtra("Name", Name);
                                                            context.startActivity(intent);
                                                        }
                                                    });

                                                    alertDialog.show();
                                                    //ds.getRef().removeValue();//remove other user's match
                                                    //matchesRef.child("email").removeValue();//remove current user's match
                                                }

                                                break;
                                            }

                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    };
                                    matchesRef.addListenerForSingleValueEvent(eventListener);
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });




                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

                builder.show();



            }
        });

        holder.btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { //show QR code
                Context context = view.getContext();
                Intent intent = new Intent(context, QRActivity.class);
                context.startActivity(intent);
                //mDatabase.child("Matches").child(currentEmail).removeValue();

            }
        });
    }


    @Override
    public int getItemCount() {
        return ChallengeListFiltered.size();
    }





}
