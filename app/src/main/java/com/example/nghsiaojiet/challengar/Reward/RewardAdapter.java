package com.example.nghsiaojiet.challengar.Reward;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Toast;

import com.example.nghsiaojiet.challengar.Challenge.Challenge;
import com.example.nghsiaojiet.challengar.Reward.Reward;
import com.example.nghsiaojiet.challengar.Reward.RewardViewHolder;
import com.example.nghsiaojiet.challengar.Matches.MatchesObject;
import com.example.nghsiaojiet.challengar.QRActivity;
import com.example.nghsiaojiet.challengar.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class RewardAdapter extends RecyclerView.Adapter<RewardViewHolder> implements Filterable {
    public ArrayList<RewardObject> RewardList = new ArrayList<RewardObject>();
    public ArrayList<RewardObject> RewardListFiltered = new ArrayList<RewardObject>();
    public Context context;

    public RewardAdapter(ArrayList<RewardObject> rewardList, Context context) {
        RewardList = rewardList;
        this.context = context;
        this.RewardListFiltered = RewardList;
        this.RewardList = RewardList;
    }


    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if(charString.isEmpty()){
                    RewardListFiltered = RewardList;
                }
                else{
                    ArrayList<RewardObject> filteredList = new ArrayList<>();
                    for(RewardObject row : RewardList){
                        if(row.getTitle().toLowerCase().contains(charString.toLowerCase())){
                            filteredList.add(row);
                        }
                    }
                    RewardListFiltered = filteredList;

                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = RewardListFiltered;
                return filterResults;

            }
            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                RewardListFiltered = (ArrayList<RewardObject>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    @NonNull
    @Override
    public RewardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(parent.getContext()).inflate(R.layout.rewarditem, parent, false);
        return new RewardViewHolder(item);
    }

    @Override
    public void onBindViewHolder(@NonNull final RewardViewHolder holder, int position) {
        final RewardObject reward = RewardListFiltered.get(position);
        holder.imgPicture.setImageResource(reward.getRewardImage());
        holder.txtTitle.setText(reward.getTitle());
        holder.txtDesc.setText(reward.getDesc());
        holder.txtQty.setText(String.valueOf(reward.getQty()));
        holder.txtptsRequired.setText(String.valueOf(reward.getPtsRequired()));

        holder.btnRedeem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = holder.getAdapterPosition();
                if (RewardList.get(position).getStatus().equals("UR")) {
                    RewardList.get(position).setStatus("R");
                    Toast.makeText(context, "You have redeemed " + RewardList.get(position).getTitle() + " ! Keep up the good work!", Toast.LENGTH_LONG).show();
                    holder.btnRedeem.setEnabled(false);
                    holder.btnRedeem.setText("Redeemed");
                } else {
                    Toast.makeText(context, "You have already redeemed " + RewardList.get(position).getTitle() + "!", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public int getItemCount () {
        return RewardListFiltered.size();
    }
}
