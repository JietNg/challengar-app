package com.example.nghsiaojiet.challengar.Reward;

public class RewardObject {
    private int rewardImage;
    private String Title;
    private String Desc;
    private int Qty;
    private int ptsRequired;
    private String Status;


    public RewardObject(String title, String desc, int qty, int ptsRequired, String status) {
        Title = title;
        Desc = desc;
        Qty = qty;
        this.ptsRequired = ptsRequired;
        Status = status;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }


    public int getRewardImage() {
        return rewardImage;
    }

    public void setRewardImage(int rewardImage) {
        this.rewardImage = rewardImage;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDesc() {
        return Desc;
    }

    public void setDesc(String desc) {
        Desc = desc;
    }

    public int getQty() {
        return Qty;
    }

    public void setQty(int qty) {
        Qty = qty;
    }

    public int getPtsRequired() {
        return ptsRequired;
    }

    public void setPtsRequired(int ptsRequired) {
        this.ptsRequired = ptsRequired;
    }

    public RewardObject(int rewardImage, String title, String desc, int qty, int ptsRequired) {
        this.rewardImage = rewardImage;
        Title = title;
        Desc = desc;
        Qty = qty;
        this.ptsRequired = ptsRequired;
    }
}
