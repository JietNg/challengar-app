package com.example.nghsiaojiet.challengar;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.nghsiaojiet.challengar.Map.MainActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Register extends AppCompatActivity {
    private Button btnRegister;
    private EditText txtName, txtPassword, txtEmail;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener firebaseAuthStateListener;
    RadioGroup gl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        btnRegister = findViewById(R.id.btnRegister);
        txtName = findViewById(R.id.txtName);
        txtPassword = findViewById(R.id.txtPassword);
        txtEmail = findViewById(R.id.txtEmail);
        gl = findViewById(R.id.RadGroup);


        mAuth = FirebaseAuth.getInstance();
        firebaseAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if(user != null){
                    Intent intent = new Intent(Register.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                    return;
                }
            }
        };

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String Name =  txtName.getText().toString();
                final String Email = txtEmail.getText().toString();
                final String Password = txtPassword.getText().toString();
                int selectId = gl.getCheckedRadioButtonId();
                final RadioButton radioButton = findViewById(selectId);
                if(radioButton.getText() == null){
                    return;
                }

                mAuth.createUserWithEmailAndPassword(Email, Password).addOnCompleteListener(Register.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(!task.isSuccessful()){
                            Toast.makeText(Register.this, "Sign up error", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            String userId = mAuth.getCurrentUser().getUid();
                            final Member member = new Member();
                            member.setName(txtName.getText().toString());
                            member.setEmail(txtEmail.getText().toString());
                            member.setPassword(txtPassword.getText().toString());
                            member.setGender(radioButton.getText().toString());
                            /*gl.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                                    if(checkedId == R.id.rbMale){
                                        member.setGender("Male");
                                    }
                                    else if(checkedId == R.id.rbFemale){
                                        member.setGender("Female");
                                    }
                                    else{
                                        Toast.makeText(Register.this, "Please select a gender!", Toast.LENGTH_SHORT).show();
                                        return;
                                    }


                                    //DatabaseReference store_Data = FirebaseDatabase.getInstance().getReference("Members");
                                    //store_Data.child(txtName.getText().toString()).setValue(member);
                                    //Intent intent = new Intent(Register.this, MainActivity.class);
                                    //startActivity(intent);
                                }
                            });*/

                            //DatabaseReference currentUserDb = FirebaseDatabase.getInstance().getReference().child("Members").child(radioButton.getText().toString()).child(userId).child("name");
                            //currentUserDb.setValue(member.getName());
                            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("Members").child(userId);
                            mDatabase.setValue(member);




                        }
                    }
                });




            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(firebaseAuthStateListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mAuth.removeAuthStateListener(firebaseAuthStateListener);
    }





}
