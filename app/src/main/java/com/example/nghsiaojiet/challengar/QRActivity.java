package com.example.nghsiaojiet.challengar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class QRActivity extends AppCompatActivity {

    Button btn_scan;
    TextView challenge_data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr);

        challenge_data = findViewById(R.id.challenge_data);
        btn_scan = findViewById(R.id.btn_scan);

        btn_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                com.google.zxing.integration.android.IntentIntegrator i = new com.google.zxing.integration.android.IntentIntegrator(QRActivity.this);
                i.setPrompt("Scan QR Code");
                i.setOrientationLocked(false);
                i.initiateScan();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult ir=IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(ir != null)
        {
            if(ir.getContents() == null)
            {
                Toast.makeText(this, "No code scan", Toast.LENGTH_SHORT).show();
            }
            else
            {
                challenge_data.setText(ir.getContents());
            }
        }
        else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


}