package com.example.nghsiaojiet.challengar.Reward;

import android.app.SearchManager;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.support.v7.widget.Toolbar;

import com.example.nghsiaojiet.challengar.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mancj.materialsearchbar.MaterialSearchBar;

import java.util.ArrayList;

public class Reward extends AppCompatActivity implements SearchView.OnQueryTextListener {
    RecyclerView recyclerView;
    RecyclerView.Adapter RewardsAdapter;
    private RewardAdapter mAdapter;
    RecyclerView.LayoutManager LayoutManager;
    private SearchView searchView;
    private ArrayList<RewardObject> rewardList = new ArrayList<RewardObject>();
    //private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;


    /*//Instantiate FireBase
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    //Get reference from FireBase
    DatabaseReference defReference = database.getReference("Rewards");
    //https://challengai-a99c2.firebaseio.com/*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reward);
        recyclerView = findViewById(R.id.recyclerView);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        RewardObject voucher = new RewardObject("Voucher", "Vouchers to redeem an orange and apple smoothie from any Boost shops.", 2, 800, "UR");
        RewardObject bag = new RewardObject("Bag", "A fashionable bag from Adidas.", 1, 10000, "UR");
        RewardObject socks = new RewardObject("Socks", "Comfortable socks for sports enthusiasts.", 5, 3000, "UR");
        RewardObject sportsShoes = new RewardObject("Sports Shoes", "Sport shoes for exercising from Nike", 1, 30000, "UR");

        rewardList.add(voucher);
        rewardList.add(bag);
        rewardList.add(socks);
        rewardList.add(sportsShoes);

        LayoutManager = new LinearLayoutManager(Reward.this);
        recyclerView.setLayoutManager(LayoutManager);
        mAdapter = new RewardAdapter(rewardList, Reward.this);
        recyclerView.setAdapter(mAdapter);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("Rewards").child("voucher").setValue(voucher);
        mDatabase.child("Rewards").child("bag").setValue(bag);
        mDatabase.child("Rewards").child("socks").setValue(socks);
        mDatabase.child("Rewards").child("sportsShoes").setValue(sportsShoes);

    }

    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.search_menu, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);
        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView)myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                mAdapter.getFilter().filter(query);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // close search view on back button pressed
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }
        super.onBackPressed();
    }


    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    public boolean onQueryTextChange(String newText) {
        return false;
    }
}