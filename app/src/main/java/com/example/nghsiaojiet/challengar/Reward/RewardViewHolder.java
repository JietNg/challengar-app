package com.example.nghsiaojiet.challengar.Reward;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nghsiaojiet.challengar.R;

public class RewardViewHolder extends RecyclerView.ViewHolder {
    public ImageView imgPicture;
    public TextView txtTitle;
    public TextView txtDesc;
    public TextView txtQty;
    public Button btnRedeem;
    public TextView txtptsRequired;
    public View v;

    public RewardViewHolder(View itemView) {
        super(itemView);
        v= itemView;
        imgPicture = v.findViewById(R.id.imgPicture);
        txtTitle = v.findViewById(R.id.txtTitle);
        txtQty = v.findViewById(R.id.txtQty);
        txtDesc = v.findViewById(R.id.txtDesc);
        btnRedeem = v.findViewById(R.id.btnRedeem);
        txtptsRequired = v.findViewById(R.id.txtptsRequired);

    }
}
