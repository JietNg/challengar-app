package com.example.nghsiaojiet.challengar.Matches;

public class MatchesObject {
    private String email;

    public MatchesObject(String email, String challenge, String firebaseToken, String name) {
        this.email = email;
        Challenge = challenge;
        this.firebaseToken = firebaseToken;
    }

    private String Challenge;
    public String firebaseToken;
    private String Name;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getFirebaseToken() {
        return firebaseToken;
    }

    public void setFirebaseToken(String firebaseToken) {
        this.firebaseToken = firebaseToken;
    }


    public MatchesObject(String email, String challenge, String firebaseToken) {
        this.email = email;
        Challenge = challenge;
        this.firebaseToken = firebaseToken;
    }




    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getChallenge() {
        return Challenge;
    }

    public void setChallenge(String challenge) {
        Challenge = challenge;
    }





}
