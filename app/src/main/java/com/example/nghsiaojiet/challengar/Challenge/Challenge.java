package com.example.nghsiaojiet.challengar.Challenge;

import java.util.ArrayList;

public class Challenge {
    private int ChallengeID;
    private int ChallengeImage;
    private String Title;
    private String Location;
    private int Pts;
    private ArrayList<String> Challengers = new ArrayList<>();



    public Challenge(int challengeImage, String title, String location, int pts, ArrayList<String> challengers) {
        ChallengeImage = challengeImage;
        Title = title;
        Location = location;
        Pts = pts;
        Challengers = challengers;
    }

    public Challenge(String title, String location, int pts, ArrayList<String> challengers) {
        Title = title;
        Location = location;
        Pts = pts;
        Challengers = challengers;
    }

    public int getChallengeImage() {
        return ChallengeImage;
    }

    public void setChallengeImage(int challengeImage) {
        ChallengeImage = challengeImage;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public int getPts() {
        return Pts;
    }

    public void setPts(int pts) {
        Pts = pts;
    }

    public ArrayList<String> getChallengers() {
        return Challengers;
    }

    public void setChallengers(ArrayList<String> challengers) {
        Challengers = challengers;
    }
}
