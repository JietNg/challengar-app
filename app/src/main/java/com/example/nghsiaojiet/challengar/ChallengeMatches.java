package com.example.nghsiaojiet.challengar;

public class ChallengeMatches {
    private String ChallengeId;
    private String MemberID;

    public ChallengeMatches(String challengeId, String memberID) {
        ChallengeId = challengeId;
        MemberID = memberID;
    }

    public String getChallengeId() {
        return ChallengeId;
    }

    public void setChallengeId(String challengeId) {
        ChallengeId = challengeId;
    }

    public String getMemberID() {
        return MemberID;
    }

    public void setMemberID(String memberID) {
        MemberID = memberID;
    }
}
