package com.example.nghsiaojiet.challengar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.codemybrainsout.onboarder.AhoyOnboarderActivity;
import com.codemybrainsout.onboarder.AhoyOnboarderCard;

import java.util.ArrayList;
import java.util.List;

public class Onboarding extends AhoyOnboarderActivity {
    List<AhoyOnboarderCard> pages = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding);
        AhoyOnboarderCard ahoyOnboarderCard1 = new AhoyOnboarderCard("Explore", "Walk around to locate challenges", R.drawable.walking);
        ahoyOnboarderCard1.setBackgroundColor(R.drawable.walking);
        ahoyOnboarderCard1.setTitleColor(R.color.white);
        ahoyOnboarderCard1.setDescriptionColor(R.color.grey_200);
        pages.add(ahoyOnboarderCard1);
        setOnboardPages(pages);
        setImageBackground(R.drawable.walking);
        setFinishButtonTitle("Let's get started");
    }

    @Override
    public void onFinishButtonPressed() {
        Intent newintent = new Intent(Onboarding.this, LoginActivity.class);
        startActivity(newintent);
    }
}
