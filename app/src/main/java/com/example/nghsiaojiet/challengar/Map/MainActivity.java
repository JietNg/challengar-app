package com.example.nghsiaojiet.challengar.Map;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;


import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.example.nghsiaojiet.challengar.Challenge.ChallengeActivity;
import com.example.nghsiaojiet.challengar.LoginActivity;
import com.example.nghsiaojiet.challengar.Matches.ChatMatches;
import com.example.nghsiaojiet.challengar.R;
import com.example.nghsiaojiet.challengar.UnityPlayerActivity;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {
//App For ChallengAR
    //Map page
    AHBottomNavigation btmNav;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btmNav = findViewById(R.id.btmNav);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //creating navitems
        AHBottomNavigationItem Profile = new AHBottomNavigationItem("Profile", R.drawable.ic_account, R.color.profile);
        AHBottomNavigationItem Chatroom = new AHBottomNavigationItem("Chat", R.drawable.ic_chat, R.color.chat);
        AHBottomNavigationItem Map = new AHBottomNavigationItem("Map", R.drawable.ic_map, R.color.map);
        AHBottomNavigationItem Reward = new AHBottomNavigationItem("Reward", R.drawable.ic_redeem, R.color.reward);
        AHBottomNavigationItem Challenge = new AHBottomNavigationItem("Challenge", R.drawable.ic_challenge, R.color.challenge);
        btmNav.addItem(Profile);
        btmNav.addItem(Chatroom);
        btmNav.addItem(Map);
        btmNav.addItem(Reward);
        btmNav.addItem(Challenge);
        btmNav.setDefaultBackgroundColor(Color.parseColor("#FEFEFE"));
        btmNav.setBehaviorTranslationEnabled(false);
        btmNav.setForceTint(true);
        btmNav.setTitleState(AHBottomNavigation.TitleState.SHOW_WHEN_ACTIVE);
        btmNav.setColored(true);
        btmNav.setCurrentItem(2);

        btmNav.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                if(position == 0){
                    Intent newintent = new Intent(MainActivity.this, com.example.nghsiaojiet.challengar.Profile.class);
                    startActivity(newintent);
                }
                else if(position == 1){
                    Intent newintent = new Intent(MainActivity.this, ChatMatches.class);
                    startActivity(newintent);
                }
                else if(position == 2){

                }
                else if(position == 3){
                    Intent newintent = new Intent(MainActivity.this, com.example.nghsiaojiet.challengar.Reward.Reward.class);
                    startActivity(newintent);
                }

                else if(position == 4){
                    Intent newintent = new Intent(MainActivity.this, ChallengeActivity.class);
                    startActivity(newintent);
                }
                return true;
            }
        });




    }

    public void onButtonClick(View view){

        Intent in = new Intent(MainActivity.this, UnityPlayerActivity.class);
        startActivity(in);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){

        MenuInflater  inflater = getMenuInflater();
        inflater.inflate(R.menu.logout_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.menuLogout:

                FirebaseAuth.getInstance().signOut();
                finish();
                startActivity(new Intent(this, LoginActivity.class));
                break;
        }

        return true;
    }
}
