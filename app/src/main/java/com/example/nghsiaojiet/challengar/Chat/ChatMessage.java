package com.example.nghsiaojiet.challengar.Chat;

import java.util.Date;

public class ChatMessage {
    private String userId;
    private String receiverID;
    private String userName;
    private String receiverName;
    private String message;
    private long timestamp;

    public ChatMessage(String userId, String receiverID, String userName, String receiverName, String message, long timestamp) {
        this.userId = userId;
        this.receiverID = receiverID;
        this.userName = userName;
        this.receiverName = receiverName;
        this.message = message;
        this.timestamp = timestamp;
    }



    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public ChatMessage(String userId) {
        this.userId = userId;
    }



    public String getReceiverID() {
        return receiverID;
    }

    public void setReceiverID(String receiverID) {
        this.receiverID = receiverID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }







}

