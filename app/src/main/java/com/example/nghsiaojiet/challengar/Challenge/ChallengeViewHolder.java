package com.example.nghsiaojiet.challengar.Challenge;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nghsiaojiet.challengar.R;

public class ChallengeViewHolder extends RecyclerView.ViewHolder {
    public ImageView imgPicture;
    public TextView txtTitle;
    public TextView txtPts;
    public TextView txtLocation;
    public Button btnStart;
    public Button btnFinish;
    public View v;

    public ChallengeViewHolder(View itemView) {
        super(itemView);
        v = itemView;
        imgPicture = v.findViewById(R.id.imgPicture);
        txtTitle = v.findViewById(R.id.txtChallenge);
        txtPts = v.findViewById(R.id.txtPts);
        txtLocation = v.findViewById(R.id.txtLocation);
        btnStart = v.findViewById(R.id.btnStart);
        btnFinish = v.findViewById(R.id.btnFinish);
    }
}
