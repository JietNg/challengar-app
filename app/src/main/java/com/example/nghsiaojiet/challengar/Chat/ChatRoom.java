package com.example.nghsiaojiet.challengar.Chat;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.example.nghsiaojiet.challengar.Matches.MatchesAdapter;
import com.example.nghsiaojiet.challengar.Matches.MatchesObject;
import com.example.nghsiaojiet.challengar.R;
import com.example.nghsiaojiet.challengar.Chat.ChatFragment;


import java.util.ArrayList;
import java.util.List;

public class ChatRoom extends AppCompatActivity{
    private Toolbar mToolbar;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mChatAdapter;
    private RecyclerView.LayoutManager mChatLayoutManager;
    private String otheruseremail;



    public static void startActivity(Context context,
                                     String receiver,
                                     String receiverUid,
                                     String firebaseToken) {
        Intent intent = new Intent(context, ChatRoom.class);
        intent.putExtra("receiver", receiver);
        intent.putExtra("receiver_uid", receiverUid);
        intent.putExtra("firebaseToken", firebaseToken);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_room);
        otheruseremail = getIntent().getStringExtra("matchEmail");
        String firebaseToken = getIntent().getStringExtra("firebase");
        String Name = getIntent().getStringExtra("matchName");
        init();


    }

    private void init(){
        setSupportActionBar(mToolbar);
        mToolbar.setTitle(getIntent().getExtras().getString("receiver"));
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout_content_chat,
                ChatFragment.newInstance(getIntent().getExtras().getString("receiver"),
                        getIntent().getExtras().getString("receiver_uid"),
                        getIntent().getExtras().getString("firebaseToken")),
                ChatFragment.class.getSimpleName());
        fragmentTransaction.commit();
    }



}
