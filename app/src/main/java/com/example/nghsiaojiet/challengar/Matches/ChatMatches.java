package com.example.nghsiaojiet.challengar.Matches;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.nghsiaojiet.challengar.Chat.ChatMessage;
import com.example.nghsiaojiet.challengar.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ChatMatches extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mMatchesAdapter;
    private RecyclerView.LayoutManager mMatchesLayoutManager;
    String otheruserEmail;
    String challengeTitle;
    String Name;
    private String currentUserID;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_matches);

        otheruserEmail = getIntent().getExtras().getString("FriendEmail");
        challengeTitle = getIntent().getExtras().getString("similarchallenge");
        Name = getIntent().getExtras().getString("Name");

        mRecyclerView = findViewById(R.id.recyclerView);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(true);


        //resultsMatches.add(new MatchesObject(otheruserEmail, challengeTitle));
        mMatchesLayoutManager = new LinearLayoutManager(ChatMatches.this);
        mRecyclerView.setLayoutManager(mMatchesLayoutManager);
        mMatchesAdapter = new MatchesAdapter(getDataSetMatches(), ChatMatches.this);
        mRecyclerView.setAdapter(mMatchesAdapter);



    }

    private ArrayList<MatchesObject> resultsMatches = new ArrayList<MatchesObject>();
    public List<MatchesObject> getDataSetMatches() {
        return resultsMatches;
    }
}
