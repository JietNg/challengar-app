package com.example.nghsiaojiet.challengar;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

public class Profile extends AppCompatActivity {

    ImageView imgPicture;
    TextView txtName;
    TextView txtEmail;
    TextView txtPassword;
    RadioGroup gl;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference ref = database.getReference();

    String uid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        txtPassword = findViewById(R.id.txtPassword);
        txtName = findViewById(R.id.txtName);
        txtEmail = findViewById(R.id.txtEmail);
        gl = findViewById(R.id.RadGroup);

        uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                txtPassword.setText(String.valueOf(dataSnapshot.child("Members").child(uid).child("password").getValue()));
                txtName.setText(String.valueOf(dataSnapshot.child("Members").child(uid).child("name").getValue()));
                txtEmail.setText(String.valueOf(dataSnapshot.child("Members").child(uid).child("email").getValue()));
                String gender = dataSnapshot.child("Members").child(uid).child("gender").getValue().toString();
                if(gender.equals("Male")){
                    gl.check(R.id.rbMale);
                    Log.d("Check gender", "onDataChange: Male");
                }
                else{
                    gl.check(R.id.rbFemale);
                    Log.d("Check gender", "onDataChange: Female");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }



}
