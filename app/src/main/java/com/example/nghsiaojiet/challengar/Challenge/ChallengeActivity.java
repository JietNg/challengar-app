package com.example.nghsiaojiet.challengar.Challenge;

import android.app.SearchManager;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v7.widget.Toolbar;

import com.example.nghsiaojiet.challengar.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class ChallengeActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    RecyclerView recyclerView;
    RecyclerView.Adapter ChallengeAdapter;
    private ChallengeAdapter mAdapter;
    RecyclerView.LayoutManager LayoutManager;
    private SearchView searchView;
    private ArrayList<Challenge> ChallengeList = new ArrayList<Challenge>();
    private ArrayList<String> Challengers = new ArrayList<String>();
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge);
        recyclerView = findViewById(R.id.recyclerView);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Challenge barCurl = new Challenge("25 Barbell Curls", "Chua Chu Kang", 15, Challengers);
        Challenge pullUps = new Challenge("20 Pull Ups", "Pasir Ris", 20, Challengers);
        Challenge marathon = new Challenge("20Km Run", "Tampines", 100, Challengers);
        Challenge run = new Challenge("20Km Run", "Tampines", 100, Challengers);

        ChallengeList.add(barCurl);
        ChallengeList.add(pullUps);
        ChallengeList.add(marathon);
        ChallengeList.add(run);

        LayoutManager = new LinearLayoutManager(ChallengeActivity.this);
        recyclerView.setLayoutManager(LayoutManager);
        ChallengeAdapter = new ChallengeAdapter(ChallengeList, ChallengeActivity.this);
        mAdapter = new ChallengeAdapter(ChallengeList, ChallengeActivity.this);
        recyclerView.setAdapter(mAdapter);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("Challenges").child("Barbell Curl").setValue(barCurl);
        mDatabase.child("Challenges").child("Pull Ups").setValue(pullUps);
        mDatabase.child("Challenges").child("Marathon").setValue(marathon);
        mDatabase.child("Challenges").child("Running").setValue(run);


    }


    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.search_menu, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);
        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView)myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                mAdapter.getFilter().filter(query);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // close search view on back button pressed
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }
        super.onBackPressed();
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }
}
