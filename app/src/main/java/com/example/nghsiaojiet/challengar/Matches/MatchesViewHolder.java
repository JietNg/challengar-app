package com.example.nghsiaojiet.challengar.Matches;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nghsiaojiet.challengar.Chat.ChatRoom;
import com.example.nghsiaojiet.challengar.Member;
import com.example.nghsiaojiet.challengar.R;

import org.w3c.dom.Text;

public class MatchesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView mMatchEmail, mMatchChallenge;
    public ImageView mMatchImage;
    public TextView mMatchToken;
    public TextView mMatchName;
    public MatchesViewHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        mMatchEmail = itemView.findViewById(R.id.MatchEmail);
        mMatchChallenge = itemView.findViewById(R.id.MatchChallenge);
        mMatchImage = itemView.findViewById(R.id.MatchImage);
        mMatchToken = itemView.findViewById(R.id.MatchToken);
        mMatchName = itemView.findViewById(R.id.MatchName);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(view.getContext(), ChatRoom.class);
        Bundle b = new Bundle();
        b.putString("matchEmail", mMatchEmail.getText().toString());
        b.putString("matchChallenge", mMatchChallenge.getText().toString());
        b.putString("firebase", mMatchToken.getText().toString()) ;
        b.putString("matchName", mMatchName.getText().toString()) ;
        intent.putExtras(b);
        view.getContext().startActivity(intent);


    }

}
